/*
class AMXTimer : public wxTimer {
public:
  int triggered;

  bool Start(int milli) {
    triggered = false;
    wxTimer::Start(milli, true);
  }

  void Notify() {
    triggered = true;
  }

  void Wait() {

  }

} timer;
*/


ClientThread *client() {
  //  OHRThread *t = dynamic_cast<OHRThread *>(wxThread::This());
  map<unsigned long, ClientThread *>::iterator i = ClientThread::client_table.find(wxThread::This()->GetId());
  if (i != ClientThread::client_table.end())
    return i->second;
  else {
    fprintf(stderr, "getClient called but id %lu not in client_table!\n", wxThread::This()->GetId());
    abort();
  }
}

void set_timer(int milli) {
  //  timer.Start(milli);
  client()->sleepamount = milli;
}

void wait_timer() {
  //  wxMillSleep(sleepamount);

  wxThread::This()->Sleep(client()->sleepamount);
}

void draw_page() {
  //  client()->window->Refresh();
  client()->pageupdated();
}

void *ClientThread::Entry() {
  //void appLoop() {
  int i, j, t = 0;

  while (!GetThread()->TestDestroy()) {
    set_timer(10);

    printf("arg"); fflush(stdout);

    for (j = 0; j < 200; j++)
      for (i = 0; i < 320; i++)
	page[j][i] = (i + j + t) % 256;

    t++;

    draw_page();

    wait_timer();
  }

    printf("entry exitting"); fflush(stdout);

}
