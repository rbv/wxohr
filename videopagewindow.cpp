// Ideally, draws either the page sent to gfx_showpage, or optionally
// any allmodex page for debugging
class ScreenPagePane : public wxPanel {
public:

  ScreenPagePane(wxWindow *parent) : wxPanel(parent) {
    thread = NULL;
    scale = 2;
    buf = new unsigned char[320 * scale * 200 * scale * 3];
    //don't waste time automatically drawing a background behind the bitmap
    SetBackgroundStyle(wxBG_STYLE_CUSTOM);
    SetMinSize(wxSize(320*scale, 200*scale));
    SetMaxSize(wxSize(320*scale, 200*scale));
    //SetVirtualSizeHints(320*scale, 200*scale, 320*scale, 200*scale);
  }

  ~ScreenPagePane() {
    delete[] buf;
  }

  void paintEvent(wxPaintEvent& evt);
  //  void paintNow();
  //  void render( wxDC& dc );

  int scale;

  unsigned char *buf;

  //unsigned char *page;

  ModeXThread *thread;

  DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(ScreenPagePane, wxPanel)
  EVT_PAINT(ScreenPagePane::paintEvent)
END_EVENT_TABLE()

void ScreenPagePane::paintEvent(wxPaintEvent &evt) {
  printf(" paint (%d)\n", evt.GetId());fflush(stdout);

  // This must always be constructed
  wxPaintDC dc(this);
  //wxBufferedPaintDC dc(this, bmp);

  //wxMutexLocker ml();

  if (!thread)
    return;

  if (thread->use_mutex.TryLock() == wxMUTEX_BUSY)
    return; //not urgent

  if (!thread->initialised) {
    thread->use_mutex.Unlock();
    return;
  }

  unsigned char *page = thread->page();
  
  if (!page) {
    printf(" err \n");fflush(stdout);
    thread->use_mutex.Unlock();
    return;
  }

  unsigned char *dptr = buf, *sptr = page;

  int i, j, k;


  // This weird blitting code maps the palette colours 0-255 onto a red to green gradient;
  // palettes not supported yet
  if (scale == 1) {
    for (i = 320 * 200; i != 0; i--) {
      dptr[0] = dptr[1] = dptr[2] = *sptr;

      dptr[0] ^= -1;
      dptr+=3;
      sptr++;
    }
  } else if (scale == 2) {
    for (j = 200; j != 0; j--) {
      for (i = 320; i != 0; i--) {
	dptr[0] = dptr[3] = ~*sptr;
	dptr[1] = dptr[2] = dptr[4] = dptr[5] = *sptr;
	dptr+=6;

	sptr++;
      }
      memcpy(dptr, dptr - 640 * 3, 640 * 3);
      dptr += 640 * 3;
    }
  }

  wxBitmap bmp(wxImage(320 * scale, 200 * scale, /*static_cast<unsigned char *>*/ buf, true));

  dc.DrawBitmap(bmp, 0, 0, false);

  thread->use_mutex.Unlock();
}
