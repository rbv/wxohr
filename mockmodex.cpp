// just pretend that FB2CPP has wrapped the revelant global variables in allmodex.bas
// in a class instantiated in TLS or whatever
/*  Moved to context.h due to sticky forward declaration problems
class AllmodexTLS {
public:
int sleepamount;
unsigned char spage[200*320];
};
*/
#define glob context()->allmx

/*
class AMXTimer : public wxTimer {
public:
  int triggered;

  bool Start(int milli) {
    triggered = false;
    wxTimer::Start(milli, true);
  }

  void Notify() {
    triggered = true;
  }

  void Wait() {

  }

} timer;
*/



void set_timer(int milli) {
  //  timer.Start(milli);
  glob.sleepamount = milli;
}

void wait_timer() {
  //  wxMillSleep(sleepamount);

  wxThread::This()->Sleep(glob.sleepamount);
}

void draw_page() {
  GFX_SHOWPAGE(glob.spage);
}

// not in allmodex

bool wantquit() {
  int t = context()->WantQuit();
  return t;
}

unsigned char *getspage() {
  return glob.spage;
}
