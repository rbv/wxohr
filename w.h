//class MainWindow : public wxFrame {
class MainWindow : public wxMDIParentFrame {
public:
  MainWindow();

  void OnClose(wxCloseEvent&);

  void MenuExit(wxCommandEvent &evt);
  void MenuAbout(wxCommandEvent &evt);
  void MenuNewThingy(wxCommandEvent &evt);
  void MenuCloseThingy(wxCommandEvent &evt);


  static wxMenuBar *CreateTopMenuBar();

private:

  enum {
    optNewTestApp = 1,
    optCloseApp,
    optSomethingLocal
  };
  
  DECLARE_EVENT_TABLE()
};


class EmbedApp : public wxApp {
 public:
  EmbedApp();

  bool OnInit();

  void KeyDwn(wxKeyEvent&);
  void KeyChar(wxKeyEvent&);

  DECLARE_EVENT_TABLE()

 private:
  MainWindow *frame;
};


DECLARE_APP(EmbedApp)
