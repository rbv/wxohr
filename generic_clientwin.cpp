
class OHRClientWindow : public wxMDIChildFrame {
public:
  OHRClientWindow(void (*entry_point)(), wxMDIParentFrame* parent, wxWindowID id, const wxString& title);


protected:
  ScreenPagePane *videopane;
  ModeXThread *thread;
  void CloseThread(wxCloseEvent &evt);


  DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(OHRClientWindow, wxMDIChildFrame)
  EVT_CLOSE(OHRClientWindow::CloseThread)
END_EVENT_TABLE()


void OHRClientWindow::CloseThread(wxCloseEvent &evt) {
  //if (evt.CanVeto())
  //  if (...) evt.Veto();
  if (thread) {
    // This needs to happen even if the thread has already stopped
    printf("waiting on thread...\n");
    // LOOK OUT! Calling Wait() on a joinable thread does NOT cause
    // TestDestroy() to return true, under Windows at least. Delete()
    // does exactly what we want under Windows, but the docs urge us not to use it.
    thread->askclose = true;
    thread->Wait();
    printf("wait done\n");
    delete thread;
  } else
    printf("OHRClientWindow didn't have a thread!\n");
  Destroy();
  printf("clientwindow:destroyed\n");
}

#if wxCHECK_VERSION(3, 0, 0)
  #define NONRESIZEABLE_FRAME  wxDEFAULT_FRAME_STYLE & ~ (wxRESIZE_BORDER | wxMAXIMIZE_BOX)
#else
  #define NONRESIZEABLE_FRAME  wxDEFAULT_FRAME_STYLE & ~ (wxRESIZE_BORDER | wxRESIZE_BOX | wxMAXIMIZE_BOX)  // wx 2.8
#endif

OHRClientWindow::OHRClientWindow(void (*entry_point)(), wxMDIParentFrame* parent, wxWindowID id, const wxString& title)
   /* Non resizeable window */ 
   : wxMDIChildFrame(parent, id, title, wxDefaultPosition, wxDefaultSize, NONRESIZEABLE_FRAME) {

  thread = new ModeXThread(entry_point);
  videopane = new ScreenPagePane(this);
  // Note: videopane won't try to draw until the thread marks it is initialised
  videopane->thread = thread;
  //videopane->page = thread->page();

  thread->window = videopane;
  thread->Run();

  //scale is currently builtin, change ScreenPagePane constructor
  wxSize sz(videopane->scale * 320, videopane->scale * 200);
  //SetVirtualSizeHints(sz, sz);
  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
  sizer->Add(videopane);
  sizer->SetSizeHints(this);
  SetSizer(sizer);
  //videopane->Show(true);
  Show(true);
}

