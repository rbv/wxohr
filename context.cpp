#include "context.h"


OHRThread::OHRThread(void (*_entry_point)())
   : wxThread(wxTHREAD_JOINABLE), entry_point(_entry_point), askclose(false) {

  Create(128*1024);
  printf("detached:%d\n", IsDetached());
}

OHRThread::~OHRThread() {
  printf("OHRThread cleanup %d\n", GetId());
}

void *OHRThread::Entry() {
  if (!entry_point) {
    printf("(OHRThread) no entry point!!\n");
    return (void *)-1;
  }
  printf("(OHRThread) entering thread\n");
  entry_point();
  printf("(OHRThread) after entry point\n");
  return (void *)0;
}

void OHRThread::OnExit(){
  printf("(OHRThread) exitting thread!\n\n");
}

//void OHRThread::SignalState(bool state) {

//}



ModeXThread::ModeXThread(void (*entry_point)()) : OHRThread(entry_point) {
  initialised = false;
  //move to gfx_wx state?
  pageupdated = false;
}

ModeXThread::~ModeXThread() {
  printf("ModeXThread deleted.\n");
}


void *ModeXThread::Entry() {
  if (!entry_point) {
    printf("(ModeXThread) no entry point!!\n");
    return (void *)-1;
  }

  // INITIALISATION

  videopage = new unsigned char[320*200];
  initialised = true;

  // WORK

  printf("(ModeXThread) entering thread\n");
  entry_point();

  // CLEANUP

  use_mutex.Lock();
  initialised = false;
  delete [] videopage;
  use_mutex.Unlock();

  printf("(ModeXThread) after entry point\n");
  //wxCloseEvent closeevt(wxID_ANY);
  //window->AddPendingEvent(closeevt);
  window->Close();
  return (void *)0;
}


void ModeXThread::PageUpdated() {
  pageupdated = true;
  if (window) {
    printf("ok");
    //Do only one of the following to avoid double drawing:
#if defined(MACOS)
    //On OSX, must throw a paint event to get smooth repainting (1-1 with paint events)
    wxPaintEvent paint;
    window->AddPendingEvent(paint);
#else
    //On winXP, must fire Refresh to get window to repaint at all.
    //OK, I admit that this does not appear to be certified thread-safe
    window->Refresh(false);
    //force immediate redraw (probably very bad think to do in a secondary thread)
    //window->Update();
#endif
  }
  else
    printf(" not ready ");
  fflush(stdout);
}




