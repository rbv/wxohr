/* An example of a program using the allmodex library */

typedef unsigned char page_t[200][320];


void myMXapp() {
  int i, j, t = 0;
  page_t &page = *(page_t *)getspage();
  int tick = 0;

  while (!wantquit()) {
    set_timer(55);

    //    printf("arg"); fflush(stdout);

    for (j = 0; j < 200; j++)
      for (i = 0; i < 320; i++)
	page[j][i] = (i + j + t) % 256;

    t++;

    draw_page();

    wait_timer();
  }

  printf("clientapp exitting\n"); fflush(stdout);

}
