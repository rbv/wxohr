SOURCE=w.cpp w.h mockmodex.cpp clientapp.cpp context.h context.cpp videopagewindow.cpp gfx_wx.cpp generic_clientwin.cpp

all: w

clean:
	rm -f w

w: ${SOURCE}
	g++ w.cpp -o w --std=c++11 `wx-config --cppflags --libs` -g

# Force wxwidgets 2.8
wx2.8: ${SOURCE}
	g++ w.cpp -o w --std=c++11 `wx-config --version=2.8 --cppflags --libs` -g
