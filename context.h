#include <map>

/* There are two types of contexts:
   -a game context
   -a video context

   All the menus operating on the same game should share a game context, consisting of, by default,
   caches and static data throughout all source files.
   Every piece of FB code that is called from C++/python will either be noninteractive, and,
   by default, get no state created for it, or will get an input/video output state created for its
   existance, which can be linked to a video panel.

   NOTE: If editting multiple games at once is not implemented, then no special game context object
         will be required.

   Prehaps try splitting all static data throughout the source into categories:
   -shareable and reentrant between all code (empty set?)
   -shareable, requiring locking (eg copy+paste buffers)
   -shareable between all code operating on the same game (requiring locking)
   -local to a single thread/menu/function call only.
*/



// Prehaps this can be removed or refactored:
// what to do about python threads which call OHR functions?
//
// It's not used directly anywhere, unsure about what functionality it should have yet
class OHRThread : public wxThread {
public:
  OHRThread(void (*_entry_point)());
  ~OHRThread();

  virtual void *Entry();
  void OnExit();

  bool WantQuit() { return askclose || TestDestroy(); }

protected:

  // unimplemented/unneeded
  //  virtual void SignalState(bool state);
  
  void (*entry_point)();
  bool askclose;
};

/*****************************************************/
class AllmodexTLS {
public:
int sleepamount;
unsigned char spage[200*320];
};



/* input/output context for a single menu/thread - is concerned mostly with allmodex and gfx_*/
/* A video context needs to manage the following:
   most of the shared variables in allmodex need to become thead-local
   some of these should be initiated by copying from an existing initialisation:
   -palette
   -font
   remove the polling thread (that'll happen in the OHR source soon)
   start/restoremodex may need to be modified, or always called on the creation of a new video
   context, or a replacement constructor supplied
   replace gfx module with C++ version with own output panel


   This class contains the state of the gfx_wx backend for some reason
*/



class ModeXThread : public OHRThread {
public:
  // Note: we don't assume that all the data for this context is initialised
  // in this constructor, when the thread starts it constructs itself and signals it's ready via 'initialised'
  ModeXThread(void (*entry_point)());

  ~ModeXThread();

  virtual void *Entry();

  AllmodexTLS allmx;


  unsigned char *page() {
    return videopage;
  }


  //Mark page as updated
  void PageUpdated();

  // Used to signal when the thread data is in use (and therefore shouldn't be freed)
  // Note that the thread itself doesn't lock this for certain tasks such as
  // updating the videopage
  wxMutex use_mutex;
  // used by other threads to see if the data is safe to use.
  bool initialised;

protected:
  friend class OHRClientWindow;
  
  //this stuff is about the BACKEND videopage, not allmodex!
  unsigned char *videopage;
  //page_t *videopage;
  bool pageupdated;
  wxWindow *window;
  //OHRClientWindow *window;
};

