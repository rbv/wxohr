#include "wx/wxprec.h"

#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

#include <cstdio>
#include <iostream>
#include <map>
using namespace std;


class ScreenPagePane : public wxPanel {
public:

  ScreenPagePane(wxFrame *parent) : wxPanel(parent) {
    SetBackgroundStyle(wxBG_STYLE_CUSTOM);
    SetMinSize(wxSize(320,200));
}

  void paintEvent(wxPaintEvent& evt);
  //  void paintNow();
  //  void render( wxDC& dc );

  unsigned char *page;
    
  DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(ScreenPagePane, wxPanel)
  EVT_PAINT(ScreenPagePane::paintEvent)
END_EVENT_TABLE()

void ScreenPagePane::paintEvent(wxPaintEvent &evt) {
  printf(" paint \n");fflush(stdout);

  if (!page) {
printf(" err \n");fflush(stdout);
    return;
  }

    wxPaintDC dc(this);

  unsigned char buf[200][320][3], *dptr = &buf[0][0][0], *sptr = page;

  int i, j, k;

  for (i = 320 * 200; i != 0; i--) {
    dptr[0] = dptr[1] = dptr[2] = *sptr;
    dptr+=3;
    sptr++;
  }


  wxBitmap bmp(wxImage(320, 200, /*static_cast<unsigned char *>*/ &buf[0][0][0], 
true));

    dc.DrawBitmap(bmp, 0, 0, false);


  //wxBufferedPaintDC dc(this, bmp);

    /*
  wxPen pen;
  for (j = 0; j < 200; j++)
    for (i = 0; i < 320; i++) {
      unsigned char c = page[i + j * 320];
      pen.SetColour(c, c, c);
      dc.SetPen(pen);

      dc.DrawPoint(i,j);
    }
    */
  //  wxColour cc;
  //dc.GetPixel(0,0,&cc);
    

  //printf(" c=%d %d\n", page[0], cc.Blue());fflush(stdout);

  //dc.Blit(0, 0, 
}

class ClientThread : public wxThreadHelper {
public:
 

  ClientThread() {
    _pageupdated = false;
    Create();
    client_table[GetThread()->GetId()] = this;
    GetThread()->Run();
    printf("detached:%d\n", GetThread()->IsDetached());
  }

  ~ClientThread() {
    printf("ClientThread cleanup %d\n", GetThread()->GetId());
    client_table.erase(GetThread()->GetId());
  }

  void *Entry();

  void pageupdated() {
    _pageupdated = true;
    if (window) {
      wxPaintEvent paint;
      printf("ok");
      //window->Refresh();
      window->AddPendingEvent(paint);
    }
    else
      printf(" not ready ");
    fflush(stdout);
  }


  static map<unsigned long, ClientThread *> client_table;

  static void waitAll() {
    while (!client_table.empty()) {
      client_table.begin()->second->GetThread()->Wait();
      client_table.erase(client_table.begin());
    }

  }

  unsigned char page[200][320];
  bool _pageupdated;
  int sleepamount;

  ScreenPagePane *window;


};

map<unsigned long, ClientThread *> ClientThread::client_table;


#include "clientapp.cpp"


/*
void *ClientThread::Entry() {

  appLoop();
}
*/

class MainWindow : public wxFrame {
public:
  MainWindow();

  void OnClose(wxCloseEvent&);

  ScreenPagePane *page;

private:
    DECLARE_EVENT_TABLE()

  ClientThread *app;

  void AddClient();


};

BEGIN_EVENT_TABLE(MainWindow, wxFrame)
  EVT_CLOSE(MainWindow::OnClose)
//  EVT_KEY_DOWN(MainWindow::KeyDwn)
//  EVT_CHAR(MainWindow::KeyChar)
END_EVENT_TABLE()

MainWindow::MainWindow() : wxFrame((wxFrame *)NULL, -1,  wxT("_"), 
				   wxDefaultPosition, wxSize(320,200)) {
  CreateStatusBar(2);
  SetStatusText(_("a"), 0);
  SetStatusText(_("b"), 1);

  wxBoxSizer *sizer = new wxBoxSizer(wxVERTICAL);
  page = new ScreenPagePane(this); //, wxID_ANY);
  sizer->Add(page, 0, 0, 0);
  sizer->SetSizeHints(this);
  SetSizer(sizer);
  page->SetFocus();


  AddClient();
}

void MainWindow::AddClient() {
  app = new ClientThread();
  page->page = (unsigned char *)&app->page;
  app->window = page;
}

void MainWindow::OnClose(wxCloseEvent& evt)
{
  printf("closing!\n");
  //ClientThread::waitAll();
  if (app)
    delete app;
  Destroy();
}


class EmbedApp : public wxApp {
public:
  MainWindow *frame;

  bool OnInit();

  void KeyDwn(wxKeyEvent&);
  void KeyChar(wxKeyEvent&);


  DECLARE_EVENT_TABLE()
};

BEGIN_EVENT_TABLE(EmbedApp, wxApp)
  EVT_KEY_DOWN(EmbedApp::KeyDwn)
  EVT_CHAR(EmbedApp::KeyChar)
END_EVENT_TABLE()


bool EmbedApp::OnInit() {
  frame = new MainWindow();
  printf("init stuff"); fflush(stdout);
  frame->Show(true);
  //  SetTopWindow(frame);
  printf("i\n");
  return true;
}


void EmbedApp::KeyDwn(wxKeyEvent &e) {
  printf("KEYDWN %d %d %c\n", e.GetKeyCode(), e.GetRawKeyCode(), e.GetKeyCode());
  e.Skip();
}

void EmbedApp::KeyChar(wxKeyEvent &e) {
  printf("KEYCHAR %d %d %c\n", e.GetKeyCode(), e.GetRawKeyCode(), e.GetKeyCode());
  e.Skip();
}


IMPLEMENT_APP(EmbedApp)


void systemcall() {

  wxArrayString output;
  wxExecute(wxT("sh -c \"ls;pwd; sleep 2\""), output);
  printf("%ls[]%ls", output[0].c_str(), output[1].c_str());

}
