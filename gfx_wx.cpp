#include <string>
using namespace std;

union RGBcolor {
  unsigned int col;
  struct {
    unsigned char b, g, r, a;
  };
};


//placeholder
template<typename t>
class FBarray {};

//this wraps whatever implementation of TLS we'll use
ModeXThread *context() {
  return static_cast<ModeXThread *>(wxThread::This());
}



void GFX_INIT() {}

void GFX_CLOSE() {}

void GFX_SHOWPAGE(unsigned char *ptr) {
  memcpy(context()->page(), ptr, 320*200);
  context()->PageUpdated();
}

//void GFX_SETPAL(FBarray<RGBcolor> pal) {
void GFX_SETPAL(RGBcolor pal[]) {

}

void GFX_WINDOWTITLE(string title) {

}

void IO_POLLKEYEVENTS() {

}

void IO_UPDATEKEYS(FBarray<int> keybd) {

}

void IO_GETMOUSE(int *mx, int *my, int *mwheel, int *mbuttons) {

}

void IO_SETMOUSE(int x, int y) {

}

int IO_READJOY(FBarray<int> joybuf, int joynum) {

}

