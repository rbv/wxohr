#include <wx/wxprec.h>

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif
#include <wx/aboutdlg.h>

#if defined(__WXMAC__) || defined(__WXCOCOA__)
  #include <ApplicationServices/ApplicationServices.h>

  #define MACOS 1
//  #define MAC_UI 1
#endif



#include <cstdio>
#include <iostream>
#include <map>
using namespace std;

//threading code, also handles all thread-local data
#include "context.cpp"
//the C++ wxwidgets graphics backend
#include "gfx_wx.cpp"
//a temporary stub for allmodex.bas, which doesn't replicate it closely at all, for testing
#include "mockmodex.cpp"
//our testing code, emulating a FB allmodex program
#include "clientapp.cpp"
//the window into which the modex page is drawn
#include "videopagewindow.cpp"
//a generic client window
#include "generic_clientwin.cpp"

//#include "key_input.cpp"

#include "w.h"



//BEGIN_EVENT_TABLE(MainWindow, wxFrame)
BEGIN_EVENT_TABLE(MainWindow, wxMDIParentFrame)
  EVT_CLOSE(MainWindow::OnClose)
//  EVT_KEY_DOWN(MainWindow::KeyDwn)
//  EVT_CHAR(MainWindow::KeyChar)

  EVT_MENU(optCloseApp,   MainWindow::MenuCloseThingy)
  EVT_MENU(optNewTestApp, MainWindow::MenuNewThingy)
  EVT_MENU(wxID_ABOUT,    MainWindow::MenuAbout)
  EVT_MENU(wxID_EXIT,     MainWindow::MenuExit)
END_EVENT_TABLE()

//MainWindow::MainWindow() : wxFrame((wxFrame *)NULL, -1,  wxT("_"), 
//				   wxDefaultPosition, wxSize(320,200)) {

MainWindow::MainWindow() : wxMDIParentFrame((wxFrame *)NULL, -1,  wxT("_"), 
				   wxDefaultPosition, wxSize(800,500)) {
  CreateStatusBar(2);
  SetStatusText(_("a"), 0);
  SetStatusText(_("b"), 1);

  SetMenuBar(CreateTopMenuBar());

  wxMDIChildFrame *child = new OHRClientWindow(myMXapp, this, -1, wxT("kid"));

}


wxMenuBar *MainWindow::CreateTopMenuBar() {
  wxMenuBar *bar = new wxMenuBar;

  wxMenu *menu = new wxMenu;
  menu->Append(optNewTestApp, wxT("Start thingy"));
  menu->AppendSeparator();
  menu->Append(wxID_EXIT, wxT("Exit"));
  bar->Append(menu, wxT("File"));

  menu = new wxMenu;
  menu->AppendSeparator();
  menu->Append(optCloseApp, wxT("Kill active"));
  bar->Append(menu, wxT("Thingy"));

  menu = new wxMenu;
  menu->Append(wxID_ABOUT, wxT("About"));
  // :)
  //  menu->FindItem(wxID_ABOUT)->SetTextColour(*wxBLUE);
  bar->Append(menu, wxT("Help"));

  return bar;
}


void MainWindow::OnClose(wxCloseEvent& evt) {
  //It's up to us to close all the frames
  wxWindowList::iterator it;
  for (it = GetChildren().begin(); it != GetChildren().end(); ) {
    wxWindow *wind = *it;
    ++it;
    //if (dynamic_cast<wxFrame *>(wind)) {
    if (wxDynamicCast(wind, wxFrame)) {
      printf("closing %s - %s\n", wind->GetName().c_str(), wind->GetLabel().c_str());
      wind->Close();
    }
  }
  printf("destroying!\n");
  Destroy();
}


void MainWindow::MenuExit(wxCommandEvent& evt) {
  Close();
}

void MainWindow::MenuAbout(wxCommandEvent &evt) {
  //INSANITY
  //Crashes under OSX if appropriate data is not packaged into .app!
  wxAboutDialogInfo info;
  //info.SetName(_("Project wxohr"));
  //  info.SetDescription(_("This is an about dialog,\n		\
  //                     part of an unnamed application."));
  //info.AddDeveloper(_("TMC is responsible"));
  wxAboutBox(info);
}

void MainWindow::MenuNewThingy(wxCommandEvent &evt) {
  new OHRClientWindow(myMXapp, this, -1, wxT("kid"));
  /*
  wxFileDialog *fileDlg = new wxFileDialog(this);
  fileDlg->ShowModal();
  fileDlg->Destroy();
  */
}

void MainWindow::MenuCloseThingy(wxCommandEvent &evt) {
  wxMDIChildFrame *child = GetActiveChild();
  if (child) {
    child->Close();
  } else
    wxBell();
}


BEGIN_EVENT_TABLE(EmbedApp, wxApp)
  EVT_KEY_DOWN(EmbedApp::KeyDwn)
  EVT_CHAR(EmbedApp::KeyChar)
END_EVENT_TABLE()

EmbedApp::EmbedApp() : frame(NULL) {}

bool EmbedApp::OnInit() {
#ifdef MACOS
  //So that the program can be interacted with even when not packaged into a .app
  ProcessSerialNumber PSN;
  GetCurrentProcess(&PSN);
  TransformProcessType(&PSN,kProcessTransformToForegroundApplication);
#endif

  //wxHandleFatalExceptions();

  printf("exit on toplevel close: %d\n", GetExitOnFrameDelete());

  //Under OSX MDIParentFrame's aren't created; their children are free floating windows, so that's jolly good
  //#ifndef MAC_UI
  frame = new MainWindow();
  frame->Show(true);
  //SetTopWindow(frame);
  //#endif

  printf("init stuff"); fflush(stdout);

  return true;
}

void EmbedApp::KeyDwn(wxKeyEvent &e) {
  printf("KEYDWN %d %d %c\n", e.GetKeyCode(), e.GetRawKeyCode(), e.GetKeyCode());
  e.Skip();
}

void EmbedApp::KeyChar(wxKeyEvent &e) {
  printf("KEYCHAR %d %d %c\n", e.GetKeyCode(), e.GetRawKeyCode(), e.GetKeyCode());
  e.Skip();
}


IMPLEMENT_APP(EmbedApp)


void systemcall() {

  wxArrayString output;
  wxExecute(wxT("sh -c \"ls;pwd; sleep 2\""), output);
  printf("%ls[]%ls", output[0].c_str(), output[1].c_str());

}
